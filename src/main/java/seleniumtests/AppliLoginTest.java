package seleniumtests;

import com.aventstack.extentreports.Status;
import common.Util;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.AppliFirstPage;
import pages.AppliHomePage;
import pages.AppliLoginPage;
import pages.AppliSearchResultPage;

import java.net.URI;

public class AppliLoginTest extends BaseTest {

    AppliLoginPage loginPage;

    @BeforeClass
    public void initPages() {
        loginPage = new AppliLoginPage(driver);
    }

    @Test(priority = 10)
    public void premierTest(ITestContext testContext) {
        currentTest.getModel().setName(browser + " : Test IHM de Top Movies");

        String title_Star_Trek = "Star Trek ";
        String title_Star_Trek_Firstcontact = "Star Trek: First Contact";
        String title_the_shawshank_Redemption = "The Shawshank Redemption";

        msg = "Step 1 : Open the application";
        driver.get(url);
        AppliLoginPage appliPage = new AppliLoginPage(driver);
        msg = "Action d'info";
        currentTest.info(msg);

        msg = "Step 1 : check a list of movie tiles is displayed";
        Assert.assertTrue(appliPage.tiles_IsDisplayed());
        currentTest.info(msg);


        msg = "Step 2 : Open The movie <b> Shawshank Redemption</b> ";
        appliPage.clicklearnButton();
        currentTest.info(msg);

        msg = "Step 2 - check the release date is correctly displayed";
        Assert.assertTrue(appliPage.inputDate_IsDisplayed());
        currentTest.log(Status.PASS, msg);


        msg = "Step 3 : Get url of the image and open";
        /*String urlImage = "http://image.tmdb.org/t/p/w342/q6y0Go1tsGEsmtFryDOJo3dEmqu.jpg";
        driver.get(urlImage); */


        msg = "Step 4 : Search <b>Stak Trek</b>";
        appliPage.clickCloseButton();
        appliPage.searchTitle(title_Star_Trek);
        currentTest.log(Status.PASS, msg);

        msg = "<b>Star Trek: First Contact</b> is displayed in the search results ";
        Assert.assertEquals(appliPage.getTitle(), title_Star_Trek_Firstcontact);
        currentTest.log(Status.PASS, msg);





    }


}
