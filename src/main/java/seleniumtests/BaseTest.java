package seleniumtests;

import com.aventstack.extentreports.ExtentTest;
import common.Util;
import extent.reporter.ExtentManager;
import extent.reporter.ExtentTestManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestContext;
import org.testng.annotations.*;

import java.util.Map;
import java.util.concurrent.TimeUnit;

public class BaseTest {
    static WebDriver driver;
    String url;
    String browser;
    String msg;
    WebDriverWait driverWait;

    static int nbFail=0;
    static ExtentTest currentTest;

    public WebDriver getDriver() {
        return driver;
    }
    public String getMsg(){
        return msg;
    }
    public ExtentTest getCurrentTest(){
        return currentTest;
    }

    /**
     * On initialise le Report ici
     */
    @BeforeSuite
    public void beforeSuite(){
        ExtentManager.getReporter();
    }

    /**
     * Dans ce template, la création du driver se fait dans le BeforeTest
     * On considère que chaque classe de Test correspond à un ensemble de scénarios qui peuvent s'enchaîner à la suite.
     * On fermera donc le driver à la fin de la classe de test, pour en ouvrir un nouveau pour la classe de test suivante.
     * @param testContext
     */
    @BeforeTest
    public void beforeTest(ITestContext testContext){
        browser = Util.getBrowser();

        /* On met en place le driver */
        String driverLocation = common.Util.getDriverLocation(browser);
        driver = common.Util.setDriver(browser, driverLocation, driver);
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

        driverWait = new WebDriverWait(driver, 30);
    }


    /**
     * On fait un startTest pour créer le test dans le Report. On utilisera l'objet currentTest pour envoyer des logs
     * startTest a besoin d'un nom par défaut, sinon, il tombe en erreur
     */
    @BeforeMethod
    public void setUp(){
        currentTest = ExtentTestManager.startTest("Test Name","Test Desc");

        /**
         * La récupération du login se fait là où on en a besoin. Si le login est le même pour tous les tests,
         * On peut très bien le récupérer dans le BeforeSuite.
         * Si on a plusieurs comptes, il vaut peut-être mieux les récupérer dans BeforeTest ou BeforeMethod, voire directement dans le Test
         */
        Map<String, String> properties = common.Util.getLoginProperties();
        url = properties.get("url");
    }

    /**
     * Après la méthode de test, on termine le test pour le Report
     */
    @AfterMethod
    public void tearDown(){
        ExtentTestManager.endTest();
    }

    /**
     * La commande driver.quit() doit se faire dans le même niveau que la création
     */
    @AfterTest
    public void afterTest(){
//        driver.quit();
    }

    /**
     * Une fois tous les tests terminés, on vérifie s'il y a des échecs. Si oui, on envoie la notification par mail.
     */
    @AfterSuite
    public void afterSuite(){
        if (getNbFail() > 0){
            Util.sendMail();
        }
    }

    public static void rien() {

    }



    public static void increaseNbFail(){
        nbFail++;
    }
    public static int getNbFail(){
        return nbFail;
    }
}
