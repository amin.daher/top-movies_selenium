package common;

import com.assertthat.selenium_shutterbug.core.Shutterbug;
import com.assertthat.selenium_shutterbug.utils.web.ScrollStrategy;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.interactions.Actions;
import org.testng.Reporter;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public interface Util {

    /**
     * Cette méthode permet de récupérer l'url et les identifiants de connexion en fonction de l'environnement et de l'application
     * @return Une HashMap contenant l'url et les identifiants de connexion
     */
    static Map<String, String> getLoginProperties(){
        HashMap<String, String> result = new HashMap<>();
        String filePath = "src/main/resources/test.properties";
        try (InputStream input = new FileInputStream(filePath)) {

            Properties prop = new Properties();
            // load a properties file
            prop.load(input);

            String url = prop.getProperty("url");


            result.put("url",url);


        } catch (IOException ex) {
            Reporter.log("IOException : \n"+ex.getMessage());
        }
        return result;
    }

    /**
     * Cette méthode permet de récupérer l'emplacement du driver
     * @param browser le navigateur spécifié dans les paramètres du test, qui permet de savoir quel driver utiliser
     * @return le chemin jusqu'au driver tel que défini dans le fichier de properties
     */
    static String getDriverLocation(String browser){
        String filePath = "src/main/resources/drivers.properties";
        String driverLocation = "";
        try (InputStream input = new FileInputStream(filePath)) {

            Properties prop = new Properties();
            prop.load(input);
            driverLocation = prop.getProperty(browser);

        } catch (IOException ex) {
            Reporter.log("IOException : \n"+ex.getMessage());
        }
        return driverLocation;
    }

    /**
     * Permet d'instancier le driver et, pour firefox, de passer l'alerte proxy si elle apparait
     * @param browser le navigateur
     * @param driverLocation l'emplacement du driver
     * @param driver le WebDriver que l'on va instancier
     * @return le WebDriver une fois instancié
     */
    static WebDriver setDriver(String browser, String driverLocation, WebDriver driver){
        if ("chrome".equals(browser)) {
            System.setProperty("webdriver." + browser + ".driver", driverLocation);
            ChromeOptions options = new ChromeOptions();
            options.setAcceptInsecureCerts(true);
            driver = new ChromeDriver(options);
        }
        else if("firefox".equals(browser)){
            System.setProperty("webdriver.gecko.driver", driverLocation);
            FirefoxOptions options = new FirefoxOptions();
            options.setAcceptInsecureCerts(true);
            driver = new FirefoxDriver(options);
        }
        return driver;
    }

    /**
     * Permet de récupérer le dossier dans lequel le report est généré
     * @return le dossier du report
     */
    static String getReportDir(){
        String filePath = "src/main/resources/common.properties";
        String reportDir = "";
        try(InputStream input = new FileInputStream(filePath)) {
            Properties prop = new Properties();
            // load a properties file
            prop.load(input);
            reportDir = prop.getProperty("surefireDir");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return reportDir;
    }

    /**
     * Permet de récupérer le nom du navigateur à utiliser
     * @return le nom du navigateur
     */
    static String getBrowser(){
        String filePath = "src/main/resources/common.properties";
        String reportDir = "";
        try(InputStream input = new FileInputStream(filePath)) {
            Properties prop = new Properties();
            // load a properties file
            prop.load(input);
            reportDir = prop.getProperty("browser");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return reportDir;
    }

    /**
     * Permet de prendre une capture d'écran de toute la page et de l'ajouter au report Extent
     * Shutterbug est un utilitaire qui permet de scroll sur une page pour prendre un capture complète, et de l'enregistrer au format PNG
     * @param driver le WebDriver
     * @param browser le navigateur, qui va constituer une partie du nom donné à la capture
     * @param msg le message à ajouter dans les logs du report
     * @param status le statut de l'étape (PASS ou FAIL)
     */
    static void capture(ExtentTest currentTest, WebDriver driver, String browser, String msg, Status status){

            String reportDir = common.Util.getReportDir();
            String picName = browser;
            String filename = picName + ".png";

            File f = new File(reportDir + "/ExtentReports/" + filename);
            int count = 1;
            while (f.exists()) {
                picName = browser + "_" + count;
                filename = picName + ".png";
                f = new File(reportDir + "/ExtentReports/" + filename);
                count++;
            }

            Shutterbug.shootPage(driver, ScrollStrategy.WHOLE_PAGE).withName(picName).save(reportDir + "/ExtentReports/");
            currentTest.log(status, msg, MediaEntityBuilder.createScreenCaptureFromPath("ExtentReports/" + filename).build());
    }


    static String decrypt(String message, String key){
        try {
            if (message==null || key==null ) return null;

            char[] keys=key.toCharArray();
            char[] mesg=new String(Base64.getDecoder().decode(message)).toCharArray();

            int ml=mesg.length;
            int kl=keys.length;
            char[] newmsg=new char[ml];
            for (int i=0; i<ml; i++){
                newmsg[i]=(char)(mesg[i]^keys[i%kl]);
            }
            mesg=null; keys=null;
            return new String(newmsg);
        }
        catch ( Exception e ) {
            return null;
        }
    }

    static void scrollToElement(WebDriver driver, WebElement element){
        Actions action = new Actions(driver);
        action.moveToElement(element).perform();
    }

    static void sendMail(){
        String app = getAppName();
        String[] recipient = getRecipient();
        String hostname = getHostName();


        try {
//            System.setProperty("mail.smtp.localhost", InetAddress.getLocalHost().getHostName()+".scapps.smtprelay.smtprelay.xpod.carrefour.com");
            System.setProperty("mail.smtp.localhost", InetAddress.getLocalHost().getHostName()+".scapps."+hostname);

            EmailAttachment attachment = new EmailAttachment();
            attachment.setPath(getReportDir()+"\\ExtentReportResults.html");
            attachment.setDisposition(EmailAttachment.ATTACHMENT);
            attachment.setDescription("Reporting");
            attachment.setName("Report.html");

            MultiPartEmail email = new MultiPartEmail();

//            email.setHostName("smtprelay.smtprelay.xpod.carrefour.com");//Azure
            email.setHostName(hostname);//Azure

//            email.addTo("vincent_lyon@carrefour.com","Vincent LYON");
//            email.addTo("simon_champenois@carrefour.com","Simon CHAMPENOIS");
//            email.addTo("christine_ducos@carrefour.com","Christine DUCOS");
            email.addTo(recipient);
            email.setFrom("vincent_lyon@carrefour.com","QoD Automate Selenium");
            email.setSubject("Automate "+app+" - KO");
            email.setMsg("Bonjour,\n\nL'automate "+app+" est KO.\nVoir le report en PJ ou sur IIS\nhttps://reportwebapp.scapps-qod.vpod1.carrefour.com/\n\nQoD");
            email.attach(attachment);
            email.send();
        } catch (UnknownHostException|EmailException e) {
            e.printStackTrace();
        }
    }

    static String getAppName(){
        String result="";
        String filePath = "src/main/resources/email.properties";
        try (InputStream input = new FileInputStream(filePath)) {

            Properties prop = new Properties();
            // load a properties file
            prop.load(input);

            result = prop.getProperty("app");


        } catch (IOException ex) {
            Reporter.log("IOException : \n"+ex.getMessage());
        }
        return result;
    }

    static String[] getRecipient(){
        String[] result = new String[20];
        String filePath = "src/main/resources/email.properties";
        try (InputStream input = new FileInputStream(filePath)) {

            Properties prop = new Properties();
            // load a properties file
            prop.load(input);

            String recipient = prop.getProperty("recipient");
            result = recipient.split(",");


        } catch (IOException ex) {
            Reporter.log("IOException : \n"+ex.getMessage());
        }
        return result;
    }

    static String getHostName(){
        String result="";
        String filePath = "src/main/resources/email.properties";
        try (InputStream input = new FileInputStream(filePath)) {

            Properties prop = new Properties();
            // load a properties file
            prop.load(input);

            result = prop.getProperty("hostname");


        } catch (IOException ex) {
            Reporter.log("IOException : \n"+ex.getMessage());
        }
        return result;
    }
}
