package extent.reporter;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ExtentManager {

    private static ExtentReports extent;

    static final File CONF = new File("src/test/resources/extent-config.xml");

    public static synchronized ExtentReports getReporter() {
        if (extent == null) {

            String filePath = "src/main/resources/common.properties";
            try(InputStream input = new FileInputStream(filePath)) {
                Properties prop = new Properties();
                // load a properties file
                prop.load(input);
                String reportDir = prop.getProperty("surefireDir");
                ExtentSparkReporter sparkReporter = new ExtentSparkReporter(reportDir + "/ExtentReportResults.html");
                sparkReporter.loadXMLConfig(CONF);
                extent = new ExtentReports();
                extent.attachReporter(sparkReporter);

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return extent;
    }
}
