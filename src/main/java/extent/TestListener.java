package extent;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import extent.reporter.ExtentManager;
import extent.reporter.ExtentTestManager;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import seleniumtests.BaseTest;

public class TestListener extends BaseTest implements ITestListener {

    private static String getTestMethodName(ITestResult iTestResult) {
        return iTestResult.getMethod().getConstructorOrMethod().getName();
    }

    @Override
    public void onStart(ITestContext iTestContext) {
        iTestContext.setAttribute("WebDriver", this.getDriver());
    }

    @Override
    public void onFinish(ITestContext iTestContext) {

        //Do tier down operations for extentreports reporting!
        ExtentTestManager.endTest();
        ExtentManager.getReporter().flush();
    }

    @Override
    public void onTestStart(ITestResult iTestResult) {
    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        Object testClass = iTestResult.getInstance();
        ExtentTest currentTest = ((BaseTest) testClass).getCurrentTest();
        currentTest.pass("Test passed");
        ExtentTestManager.endTest();
    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {

        increaseNbFail();
        //Get driver from BaseTest and assign to local webDriver variable.
        Object testClass = iTestResult.getInstance();
        ExtentTest currentTest = ((BaseTest) testClass).getCurrentTest();
        WebDriver webDriver = ((BaseTest) testClass).getDriver();
        Capabilities cap = ((RemoteWebDriver) webDriver).getCapabilities();
        String browser = cap.getBrowserName();
        String msg = ((BaseTest) testClass).getMsg();
        msg = msg + ". See Error message : \r\n" + iTestResult.getThrowable().getMessage();
        common.Util.capture(currentTest, webDriver, browser, msg, Status.FAIL);
        ExtentTestManager.endTest();
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {
        Object testClass = iTestResult.getInstance();
        ExtentTest currentTest = ((BaseTest) testClass).getCurrentTest();
        //ExtentReports log operation for skipped tests.
        currentTest.getModel().setName(getTestMethodName(iTestResult));

        currentTest.log(Status.SKIP, "Test Skipped : "+getTestMethodName(iTestResult)+" --- \r\n"+iTestResult.getThrowable().getMessage());
        ExtentTestManager.endTest();
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
    }
}
