package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AppliFirstPage extends BasePage {
    public AppliFirstPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(xpath= "//*[@id=\"sp-cc-accept\"]")
    private WebElement cookieButton;




    @FindBy(xpath = "//*[@id=\"nav-signin-tooltip\"]/a/span")
    private WebElement identifierButon;

    @FindBy(id = "data-account")
    private WebElement compteButton;

    public void clickCookieButton() {
        cookieButton.click();
    }

    public void clickIdentifierButton() {
        identifierButon.click();
    }




}
