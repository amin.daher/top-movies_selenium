package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class AppliSearchResultPage extends BasePage {
    public AppliSearchResultPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(id = "//*[@id=\"products\"]/section/div[1]/div[3]/div/div[2]/h5/span/span")
    private WebElement resultButton;

    @FindBy(xpath = "//*[@id=\"data-nav\"]/div[3]/div[2]/button")
    private WebElement fermerButton;


    public void clickFermerButton() {
        fermerButton.click();
    }
    public void checkResult() {
//        Assert.assertTrue(resultButton.getText().contains("125 résultats"));
    }
//   public String getResult(){
//        String affichage = resultButton.getText();
//        return affichage;
//   }



}
