package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AppliLoginPage extends BasePage {
    public AppliLoginPage(WebDriver webDriver) {
        super(webDriver);
    }

    WebDriverWait driverWait = new WebDriverWait(driver, 30);


    @FindBy(xpath = "//h2[text()='The Shawshank Redemption'] /parent:: div / parent :: div //button")
    private WebElement learnMoreButton;

    @FindBy(xpath = "//input[@value='1994-09-23']")
    private WebElement inputReleaseDate;

    @FindBy(xpath = "//span[text()='Close']")
    private WebElement closeButton;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement searchInput;

    @FindBy(xpath = "//div[@class='jss89 movie-image']")
    private WebElement movieCard;

    @FindBy(xpath = "//h2[text()='Star Trek: First Contact']")
    private WebElement titlemovie;
    public String getTitle() {
        String txt = titlemovie.getText();
        System.out.println("lllll" +txt);
        return txt;
    }

    public boolean tiles_IsDisplayed(){
        return  movieCard.isDisplayed();
    }

    public void clicklearnButton() {
        learnMoreButton.click();
    }

   public boolean inputDate_IsDisplayed() {
     driverWait.until((ExpectedConditions.visibilityOf(inputReleaseDate)));
     return inputReleaseDate.isDisplayed();
   }

   public void clickCloseButton() {
        closeButton.click();
   }

   public void searchTitle(String title) {
        searchInput.click();
        searchInput.sendKeys(title);
        searchInput.sendKeys(Keys.ENTER);

   }



}
