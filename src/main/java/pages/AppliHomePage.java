package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;


public class AppliHomePage extends BasePage {

    public AppliHomePage(WebDriver webdriver) {
        super(webdriver);
    }


    @FindBy(id = "twotabsearchtextbox")
    private WebElement InputProduct;

    @FindBy(id = "nav-search-submit-button")
    private WebElement searchButton;




    public void selectProduit(String produit) {
        InputProduct.sendKeys(produit);
    }


    public void clickSeachButton() {
        searchButton.click();

    }


}
