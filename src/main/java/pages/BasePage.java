package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class BasePage {

    protected WebDriver driver;

    public BasePage(WebDriver webDriver){
        this.driver = webDriver;
//        PageFactory.initElements(driver, this);
        PageFactory.initElements(new AjaxElementLocatorFactory(webDriver,10), this);
    }

    public WebDriver getDriver() {
        return driver;
    }
}
